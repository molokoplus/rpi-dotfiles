# If you come from bash you might have to change your $PATH.
export PATH=$HOME/.local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/pi/.oh-my-zsh"

ZSH_THEME="robbyrussell"

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
export LANG=es_AR.UTF-8

export EDITOR='vim'

source .env

# Alias
alias ctorrents='cd $TORRENT/complete' 
alias itorrents='cd $TORRENT/incoming' 
alias movies='cd $MEDIA/peliculas'
alias series='cd $MEDIA/series'
alias getsub='subliminal download -l es -p subscenter -p thesubdb -p shooter -p tvsubtitles -p addic7ed -p legendastv -p opensubtitles -p podnapisi'

